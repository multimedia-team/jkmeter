// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2018 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#include <string.h>
#include <sndfile.h>
#include <math.h>
#include "jclient.h"


Jclient::Jclient (const char *jname, const char *jserv, int chan, int corr) :
    A_thread ("Jclient"),
    _jack_client (0),
    _fsamp (0),
    _fsize (0),
    _active (false),
    _kmdsp (0),
    _chan (chan),
    _corr (corr)
{
    init_jack (jname, jserv);   
    initialise ();
    _active = true;
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
}


void Jclient::init_jack (const char *jname, const char *jserv)
{
    int            i;
    char           t [16];
    jack_status_t  stat;
    int            opts;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    if ((_jack_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK.\n");
        exit (1);
    }
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);
    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }

    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);
    _fsize = jack_get_buffer_size (_jack_client);

    for (i = 0; i < _chan; i++)
    {
	sprintf (t, "in-%d", i + 1);
        _jack_ipports [i] = jack_port_register (_jack_client, t, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    }

    _kmdsp = new Kmeterdsp [_chan];
}


void Jclient::close_jack ()
{
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
    delete[] _kmdsp;
}


void Jclient::jack_static_shutdown (void *arg)
{
    return ((Jclient *) arg)->jack_shutdown ();
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}
 

int Jclient::jack_process (jack_nframes_t nframes)
{
    int   i;
    float *p0, *p1;

    if (! _active) return 0;

    if (_fsize)
    {
	p0 = (float *) jack_port_get_buffer (_jack_ipports [0], nframes);
	p1 = (float *) jack_port_get_buffer (_jack_ipports [1], nframes);
        _kmdsp [0].process (p0, nframes);
        _kmdsp [1].process (p1, nframes);
	if (_corr) _scdsp.process (p0, p1, nframes);
	for (i = 2; i < _chan; i++)
	{
	     p0 = (float *) jack_port_get_buffer (_jack_ipports [i], nframes);
            _kmdsp [i].process (p0, nframes);
	}
    }
    return 0;
}


void Jclient::initialise ()
{
    Kmeterdsp::init (_fsamp, _fsize, 0.5f, 15.0f);
    Stcorrdsp::init (_fsamp, 1e3f, 0.15f);
}

