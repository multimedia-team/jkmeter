// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2018 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "png2img.h"
#include "cmeter.h"


XImage *Cmeter::_stc_meterH0 = 0;
XImage *Cmeter::_stc_meterH1 = 0;
XImage *Cmeter::_stc_meterV0 = 0;
XImage *Cmeter::_stc_meterV1 = 0;
XImage *Cmeter::_stc_scaleH  = 0;
XImage *Cmeter::_stc_scaleV  = 0;



Cmeter::Cmeter (X_window *parent, int xpos, int ypos, int geom) :
    X_window (parent, xpos, ypos, LINEW, LINEW, 0),
    _geom (geom),
    _xs (0),
    _ys (0),
    _k (1),
    _pixm (0),
    _imag0 (0),
    _imag1 (0)
{
    if (geom == HOR) 
    {
	_imag0 = _stc_meterH0;
        _imag1 = _stc_meterH1;
    }
    else if (geom == VER)
    {
	_imag0 = _stc_meterV0;
        _imag1 = _stc_meterV1;
    }
    if (!_imag0 || !_imag1) return;

    _xs = _ys = LINEW;
    switch (geom)
    {
    case HOR:
	_xs = _imag0->width;
	break;
    case VER:
	_ys = _imag0->height;
	break;
    default:
	return;
    }

    _pixm = XCreatePixmap (dpy (), win (), _xs, _ys, disp ()->depth ());
    if (! _pixm) return;
    XPutImage (dpy (), _pixm, dgc (), _imag0, 0, 0, 0, 0, _xs, _ys); 
    XSetWindowBackgroundPixmap (dpy (), win (), _pixm);
    x_resize (_xs, _ys);
}


Cmeter::~Cmeter (void)
{
    if (_pixm) XFreePixmap (dpy (), _pixm);
}


void Cmeter::update (float v)
{
    int k;

    if (_geom == HOR)
    {
        k = (int)(71.5f + 60.0f * v);
        if (k != _k)
	{
            XPutImage (dpy (), _pixm, dgc (), _imag0, _k - 1, 0, _k - 1, 0, 3, LINEW); 
	    _k = k;
            XPutImage (dpy (), _pixm, dgc (), _imag1, _k - 1, 0, _k - 1, 0, 3, LINEW); 
	}
    }
    else
    {
        k = (int)(71.5f - 60.0f * v);
        if (k != _k)
	{
            XPutImage (dpy (), _pixm, dgc (), _imag0, 0, _k - 1, 0, _k - 1, LINEW, 3); 
	    _k = k;
            XPutImage (dpy (), _pixm, dgc (), _imag1, 0, _k - 1, 0, _k - 1, LINEW, 3); 
	}
    }
    x_clear ();
}


void Cmeter::load_images (X_display *disp, const char *path)
{
    char s [1024];

    if (_stc_meterH0) return;

    snprintf (s, 1024, "%s/stc-meterH0.png", path);
    _stc_meterH0 = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/stc-meterH1.png", path);
    _stc_meterH1 = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/stc-meterV0.png", path);
    _stc_meterV0 = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/stc-meterV1.png", path);
    _stc_meterV1 = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/stc-scaleH.png", path);
    _stc_scaleH = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/stc-scaleV.png", path);
    _stc_scaleV = png2img (s, disp, 0);
}
