// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2020 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "png2img.h"
#include "kmeter.h"


unsigned long Kmeter::_peakcol = 0;
XImage *Kmeter::_k20_meterH0 = 0;
XImage *Kmeter::_k20_meterH1 = 0;
XImage *Kmeter::_k20_meterV0 = 0;
XImage *Kmeter::_k20_meterV1 = 0;
XImage *Kmeter::_k20_scaleH  = 0;
XImage *Kmeter::_k20_scaleV  = 0;
XImage *Kmeter::_k14_meterH0 = 0;
XImage *Kmeter::_k14_meterH1 = 0;
XImage *Kmeter::_k14_meterV0 = 0;
XImage *Kmeter::_k14_meterV1 = 0;
XImage *Kmeter::_k14_scaleH  = 0;
XImage *Kmeter::_k14_scaleV  = 0;
XImage *Kmeter::_k12_meterH0 = 0;
XImage *Kmeter::_k12_meterH1 = 0;
XImage *Kmeter::_k12_meterV0 = 0;
XImage *Kmeter::_k12_meterV1 = 0;
XImage *Kmeter::_k12_scaleH  = 0;
XImage *Kmeter::_k12_scaleV  = 0;


Kmeter::Kmeter (X_window *parent, int xpos, int ypos, int geom, int kval) :
    X_window (parent, xpos, ypos, LINEW, LINEW, 0),
    _geom (geom),
    _kval (kval),
    _xs (0),
    _ys (0),
    _kr (-1),
    _kp (-1),
    _dp (0),
    _pixm (0),
    _imag0 (0),
    _imag1 (0)
{
    switch (kval)
    {
    case K20:
        _imag0 = (geom == HOR) ? _k20_meterH0 : _k20_meterV0; 
        _imag1 = (geom == HOR) ? _k20_meterH1 : _k20_meterV1; 
        _scale = (geom == HOR) ? _k20_scaleH : _k20_scaleV; 
	break;
    case K14:
        _imag0 = (geom == HOR) ? _k14_meterH0 : _k14_meterV0; 
        _imag1 = (geom == HOR) ? _k14_meterH1 : _k14_meterV1; 
        _scale = (geom == HOR) ? _k14_scaleH  : _k14_scaleV; 
	break;
    case K12:
        _imag0 = (geom == HOR) ? _k12_meterH0 : _k12_meterV0; 
        _imag1 = (geom == HOR) ? _k12_meterH1 : _k12_meterV1; 
        _scale = (geom == HOR) ? _k12_scaleH  : _k12_scaleV; 
	break;
    default:
	return;
    }
    if (!_imag0 || !_imag1) return;

    _xs = _ys = LINEW;
    switch (geom)
    {
    case HOR:
	_xs = _imag0->width;
	break;
    case VER:
	_ys = _imag0->height;
	break;
    default:
	return;
    }

    _pixm = XCreatePixmap (dpy (), win (), _xs, _ys, disp ()->depth ());
    if (! _pixm) return;
    XPutImage (dpy (), _pixm, dgc (), _imag0, 0, 0, 0, 0, _xs, _ys); 
    XSetWindowBackgroundPixmap (dpy (), win (), _pixm);
    x_resize (_xs, _ys);
}


Kmeter::~Kmeter (void)
{
    if (_pixm) XFreePixmap (dpy (), _pixm);
}


void Kmeter::update (float r, float p)
{
    int x, y, kr, kp, dp;

    switch (_kval)
    {
    case K20:
        kr = (int)(mapk20 (r) + 0.5f);
        kp = (int)(mapk20 (p) + 0.5f);
	break;
    case K14:
        kr = (int)(mapk14 (r) + 0.5f);
        kp = (int)(mapk14 (p) + 0.5f);
	break;
    case K12:
        kr = (int)(mapk12 (r) + 0.5f);
        kp = (int)(mapk12 (p) + 0.5f);
	break;
    default:
	return;
    }	
	
    dp = kp - kr;
    if (dp >  3) dp = 3;
    if (kp < 24) dp = 0;
    XSetForeground (dpy (), dgc (), _peakcol);
    if (_geom == HOR)
    {
        if (_dp > 0)
	{
	    x = _kp + 11;
            XPutImage (dpy (), _pixm, dgc (), _imag0, x - _dp, 0, x - _dp, 0, _dp, LINEW);
	}
	if (kr > _kr)
	{
	    x = _kr + 11;
            XPutImage (dpy (), _pixm, dgc (), _imag1, x, 0, x, 0, kr - _kr, LINEW); 
	}	    
	else if (kr < _kr)
	{
	    x = kr + 11;
            XPutImage (dpy (), _pixm, dgc (), _imag0, x, 0, x, 0, _kr - kr, LINEW); 
	}	    
	if (dp > 0)
	{
	    x = kp + 11;
            XFillRectangle (dpy (), _pixm, dgc (), x - dp, 0, dp, LINEW);
	}
    }
    else
    {
        if (_dp > 0)
	{
	    y = _ys - 11- _kp;
            XPutImage (dpy (), _pixm, dgc (), _imag0, 0, y, 0, y, LINEW, _dp);
	}
	if (kr > _kr)
	{
	    y = _ys - 11 - kr;
            XPutImage (dpy (), _pixm, dgc (), _imag1, 0, y, 0, y, LINEW, kr - _kr); 
	}	    
	else if (kr < _kr)
	{
	    y = _ys - 11 - _kr;
            XPutImage (dpy (), _pixm, dgc (), _imag0, 0, y, 0, y, LINEW, _kr - kr); 
	}	    
	if (dp > 0)
	{
	    y = _ys - 11 - kp;
            XFillRectangle (dpy (), _pixm, dgc (), 0, y, LINEW, dp);
	}
    }
    _kr = kr;
    _kp = kp;
    _dp = dp;
    x_clear ();
}


float Kmeter::mapk20 (float v)
{
    if (v < 1e-3f) return 24e3f * v;
    v = log10f (v) + 3.0f;
    if (v < 2.0f) return 24 + v * (100 + v * 16);
    if (v > 3.0f) v = 3.0f;
    return v * 160 - 32;
}


float Kmeter::mapk14 (float v)
{
    if (v < 2e-3f) return 10e3f * v;
    v = log10f (v) + 2.7f;
    if (v < 2.0f) return 20 + v * (80 + v * 32);
    if (v > 2.7f) v = 2.7f;
    return v * 200 - 92;
}

 
float Kmeter::mapk12 (float v)
{
    if (v < 2.5e-3f) return 8e3f * v;
    v = log10f (v) + 2.6f;
    if (v < 2.0f) return 20 + v * (90 + v * 32);
    if (v > 2.7f) v = 2.7f;
    return v * 200 - 72;
}

 
void Kmeter::load_images (X_display *disp, const char *path)
{
    if (_k20_meterH0) return;
    _peakcol = disp->whitepixel ();

    _k20_meterH0 = read_png ("k20-meterH0", path, disp);
    _k20_meterH1 = read_png ("k20-meterH1", path, disp);
    _k20_meterV0 = read_png ("k20-meterV0", path, disp);
    _k20_meterV1 = read_png ("k20-meterV1", path, disp);
    _k20_scaleH = read_png ("k20-scaleH", path, disp);
    _k20_scaleV = read_png ("k20-scaleV", path, disp);

    _k14_meterH0 = read_png ("k14-meterH0", path, disp);
    _k14_meterH1 = read_png ("k14-meterH1", path, disp);
    _k14_meterV0 = read_png ("k14-meterV0", path, disp);
    _k14_meterV1 = read_png ("k14-meterV1", path, disp);
    _k14_scaleH = read_png ("k14-scaleH", path, disp);
    _k14_scaleV = read_png ("k14-scaleV", path, disp);

    _k12_meterH0 = read_png ("k12-meterH0", path, disp);
    _k12_meterH1 = read_png ("k12-meterH1", path, disp);
    _k12_meterV0 = read_png ("k12-meterV0", path, disp);
    _k12_meterV1 = read_png ("k12-meterV1", path, disp);
    _k12_scaleH = read_png ("k12-scaleH", path, disp);
    _k12_scaleV = read_png ("k12-scaleV", path, disp);
}


XImage* Kmeter::read_png (const char *file, const char *path, X_display *disp)
{
    char     s [1024];
    XImage   *I;
    
    snprintf (s, 1024, "%s/%s.png", path, file);
    I = png2img (s, disp, 0);
    if (I) return I;
    fprintf (stderr, "Can't load '%s'.\n", s);
    exit (1);
}
    
