// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2021 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#ifndef __MAINWIN_H
#define	__MAINWIN_H


#include <clxclient.h>
#include "global.h"
#include "jclient.h"
#include "kmeter.h"
#include "cmeter.h"



class Mainwin : public A_thread, public X_window, public X_callback
{
public:

    Mainwin (X_rootwin *parent, X_resman *xres, Jclient *jclien);
    ~Mainwin (void);
    Mainwin (const Mainwin&);
    Mainwin& operator=(const Mainwin&);

    void stop (void) { _stop = true; }
    int  process (void);

private:

    virtual void thr_main (void) {}

    void handle_time (void);
    void handle_stop (void);
    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void expose (XExposeEvent *E);
    void clmesg (XClientMessageEvent *E);
    void addtext (X_window *W, X_textln_style *T, int xp, int yp, int xs, int ys, const char *text, int align);
    void update1 (int k);
    void update2 (void);
    void redraw (void);
    void makegui (void);


    X_resman       *_xres;
    Atom            _atom;
    int             _xs;
    int             _ys;
    bool            _stop;
    bool            _vert;
    int             _kval;
    int             _chan;
    int             _corr;
    int             _time;
    Jclient        *_jclient;
    Kmeter         *_meters [MAXCHAN];
    Cmeter         *_cmeter;
    X_tbutton      *_pkbutt;
    float           _peak1;
    float           _peak2;
};


#endif
