// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2018 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#include "styles.h"


XftColor        *XftColors [NXFTCOLORS];
XftFont         *XftFonts  [NXFTFONTS];
X_button_style  Bst0;



XftColor *blackorwhite (XftColor *C)
{
    float v;

    v = 0.23f * C->color.red + 0.70f * C->color.green + 0.07f * C->color.blue;
    return (v > 25e3f) ? XftColors [C_BLACK] : XftColors [C_WHITE];
}


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_BLACK]    = disp->alloc_xftcolor ("black",  0);
    XftColors [C_WHITE]    = disp->alloc_xftcolor ("white",  0);
    XftColors [C_MAIN_BG]  = disp->alloc_xftcolor ("gray10", 0);
    XftColors [C_MAIN_FG]  = disp->alloc_xftcolor ("white",  0);
    XftColors [C_MAIN_LS]  = disp->alloc_xftcolor ("gray70", 0);
    XftColors [C_MAIN_DS]  = disp->alloc_xftcolor ("gray30", 0);

    XftFonts [F_BUTT] = disp->alloc_xftfont (xrm->get (".font.butt", "luxi:bold:pixelsize=9"));

    Bst0.font = XftFonts [F_BUTT];
    Bst0.type = X_button_style::PLAIN;
    Bst0.color.bg[0] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[0] = disp->alloc_xftcolor ("#8080ff", 0);
    Bst0.color.bg[1] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[1] = disp->alloc_xftcolor ("#40ff40", 0);
    Bst0.color.bg[2] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[2] = disp->alloc_xftcolor ("#ffff00", 0);
    Bst0.color.bg[3] = disp->alloc_color ("#ff2000", 0);
    Bst0.color.fg[3] = disp->alloc_xftcolor ("#ffffff", 0);
}


void styles_fini (X_display *disp)
{
}


