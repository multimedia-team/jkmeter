// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2021 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <clthreads.h>
#include <jack/jack.h>
#include "global.h"
#include "kmeterdsp.h"
#include "stcorrdsp.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv, int chan, int corr);
    ~Jclient (void);

    int           chan (void) const { return _chan; }
    const char   *jname (void) const { return _jname; }
    unsigned int  fsamp (void) const { return _fsamp; }
    Kmeterdsp    *kmdsp (int i) const { return (Kmeterdsp *)_kmdsp + i; }
    Stcorrdsp    *scdsp (void) { return (Stcorrdsp *) &_scdsp; }

private:

    void init_jack (const char *jname, const char *jserv);
    void close_jack (void);
    void jack_shutdown (void);
    int  jack_process (jack_nframes_t nframes);

    virtual void thr_main (void) {}

    void initialise (void);

    jack_client_t  *_jack_client;
    jack_port_t    *_jack_ipports [MAXCHAN];
    const char     *_jname;
    unsigned int    _fsamp;
    unsigned int    _fsize;
    bool            _active;
    Kmeterdsp      *_kmdsp;
    Stcorrdsp       _scdsp;
    int             _chan;
    int             _corr;

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
