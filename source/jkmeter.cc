// -----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2021 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <sys/mman.h>
#include <signal.h>
#include "styles.h"
#include "mainwin.h"

#define CP (char *)
#define NOPTS 11


XrmOptionDescRec options [NOPTS] =
{ 
    { CP"-help",    CP".help",            XrmoptionNoArg,   CP"true"   },
    { CP"-name",    CP".name",            XrmoptionSepArg,  0          },
    { CP"-type",    CP".type",            XrmoptionSepArg,  CP"k20"    },
    { CP"-rate",    CP".rate",            XrmoptionSepArg,  CP"25"     },
    { CP"-chan",    CP".channels",        XrmoptionSepArg,  0          },
    { CP"-C",       CP".correlation",     XrmoptionNoArg,   CP"true"   },
    { CP"-V",       CP".vertical",        XrmoptionNoArg,   CP"true"   },
    { CP"-H",       CP".horizontal",      XrmoptionNoArg,   CP"true"   },
    { CP"-O",       CP".orientation",     XrmoptionSepArg,  0          },
    { CP"-s",       CP".server",          XrmoptionSepArg,  0          },
    { CP"-g",       CP".geometry",        XrmoptionSepArg,  0          }
};


static Jclient  *jclient = 0;
static Mainwin  *mainwin = 0;


static void help (void)
{
    fprintf (stderr, "\n%s-%s\n", PROGNAME, VERSION);
    fprintf (stderr, "(C) 2008-2021 Fons Adriaensen  <fons@linuxaudio.org>\n");
    fprintf (stderr, "Syntax: jkmeter <options>\n");
    fprintf (stderr, "Options:\n");
    fprintf (stderr, "  -h             Display this text\n");
    fprintf (stderr, "  -name <name>   Jack client name\n");
    fprintf (stderr, "  -s <server>    Jack server name\n");
    fprintf (stderr, "  -g <geometry>  Window position\n");
    fprintf (stderr, "  -type <type>   Meter type, k20, k14 or k12 (k20)\n");
    fprintf (stderr, "  -rate <rate>   Update rate (25)\n");
    fprintf (stderr, "  -chan <chan>   Number of channels (2)\n");   
    fprintf (stderr, "  -C             Add correlation meter\n");   
    fprintf (stderr, "  -V             Vertical layout\n");   
    fprintf (stderr, "  -H             Horizontal layout\n");   
    exit (1);
}


static void sigint_handler (int)
{
    signal (SIGINT, SIG_IGN);
    mainwin->stop ();
}


int main (int ac, char *av [])
{
    X_resman      xresman;
    X_display     *display;
    X_handler     *handler;
    X_rootwin     *rootwin;
    int           ev, chan, corr;

    xresman.init (&ac, av, CP"jkmeter", options, NOPTS);
    if (xresman.getb (".help", 0)) help ();
            
    display = new X_display (xresman.get (".display", 0));
    if (display->dpy () == 0)
    {
	fprintf (stderr, "Can't open display.\n");
        delete display;
	exit (1);
    }

    chan = atoi (xresman.get (".channels", "2"));
    if (chan <  1) chan =  1;
    if (chan > MAXCHAN) chan = MAXCHAN;

    corr = (xresman.getb (".correlation", 0));
    Kmeter::load_images (display, SHARED);
    Cmeter::load_images (display, SHARED);
    styles_init (display, &xresman);
    rootwin = new X_rootwin (display);
    jclient = new Jclient (xresman.rname (), xresman.get (".server", 0), chan, corr);
    mainwin = new Mainwin (rootwin, &xresman, jclient);
    handler = new X_handler (display, mainwin, EV_X11);
    handler->next_event ();
    mainwin->x_map ();
    XFlush (display->dpy ());

    ITC_ctrl::connect (jclient, EV_EXIT, mainwin, EV_EXIT);
    if (mlockall (MCL_CURRENT | MCL_FUTURE)) fprintf (stderr, "Warning: memory lock failed.\n");
    signal (SIGINT, sigint_handler); 

    do
    {
	ev = mainwin->process ();
	if (ev == EV_X11)
	{
	    rootwin->handle_event ();
	    handler->next_event ();
	}
    }
    while (ev != EV_EXIT);	

    delete jclient;
    delete handler;
    delete rootwin;
    delete display;
   
    return 0;
}



